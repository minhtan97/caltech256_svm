# Caltech256_SVM
----
# Introduction
----
This project use caltech256 dataset with VGG16 architecture to extract feature and SVM Linear algorithm to detect object.
# Requirement
----
Using [this script](https://bitbucket.org/minhtan97/caltech256_svm/src/master/requirement.txt) to install indispensable libs:
```
sudo chmod +x requirement.txt
./requirement.txt
```
# Result
----
#### Accuracy of model db1:
![Accuracy of model db1](result/db1.png)
#### Accuracy of model db2:
![Accuracy of model db2](result/db2.png)
#### Accuracy of model db3:
![Accuracy of model db2](result/db3.png)

# Directory Structure
----
```
Caltech256
-db
--db1
---train.txt
---text.txt
--...
-exp
--knn
--svmlinear
---db1
----ketqua.txt
----model.joblib
---...
-feature
--vgg16_fc2
---256 classes
-images
--256 classes
-test_predict
--test image
-extract_features.py
-generate_db.py
-predict.py
-train.py
-test.py
```
# Usage
----
## 1. Caltech dataset:
Click this [link](http://www.vision.caltech.edu/Image_Datasets/Caltech256/256_ObjectCategories.tar) to download caltech dataset, extract it and put into folder caltech256.
## 2. Generate training set and test set:
Using [this script](https://bitbucket.org/minhtan97/caltech256_svm/src/master/generate_db.py) to generate training set and test set as following command:
```
python3 generate_db.py images db/db1
```
It will create train.txt and test.txt in folder db/db1
## 3. Feature extraction:
Using [this script](https://bitbucket.org/minhtan97/caltech256_svm/src/master/extract_features.py) to extract feature of images. The systax as following:
#### To extract feature of training set:
```
python3 extract_features.py db/db1/train.txt
```
#### To extract feature of test set:
```
python3 extract_features.py db/db1/test.txt
```
## 4. Training:
Using [this script](https://bitbucket.org/minhtan97/caltech256_svm/src/master/train.py) to train model with SVM linear algorithm. The syntax as following:
```
python3 train.py db/db1/train.txt db/db1/test.txt exp/svmlinear/db1
```
It will create model.joblib in folder exp/svmlinear/db1.
## 5. Testing:
Using [this script](https://bitbucket.org/minhtan97/caltech256_svm/src/master/test.py) to test model. The syntax as following:
```
python3 test.py db/db1/test.txt exp/svmlinear/db1
```
It will create ketqua.txt in folder exp/svmlinear/db1. This file is the label result after using trained model to detect.
## 6. Testing with your owm image:
Using [this script](https://bitbucket.org/minhtan97/caltech256_svm/src/master/predict.py) to predict your owm image with trained model. The syntax as following:
```
python3 predict.py <PATH_OF_IMAGE> exp/svmlinear/db1
```
It will print label of your image in terminal.

# References
----

## Documentations

* [Support Vector Machines with scikit-learn](https://www.datacamp.com/community/tutorials/svm-classification-scikit-learn-python)
* [Support Vector Machines](http://scikit-learn.org/stable/modules/svm.html)
