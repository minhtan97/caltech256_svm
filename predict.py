from keras.preprocessing import image
from keras.applications.vgg16 import VGG16
from keras.models import Model
from keras.models import load_model
from keras.applications.vgg16 import preprocess_input
from sklearn import svm
from sklearn.externals import joblib
import numpy as np
import sys
import os
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES=True
print("[+] Setup model")
base_model = VGG16(weights='imagenet', include_top=True)
out = base_model.get_layer("fc2").output
model = Model(inputs=base_model.input, outputs=out)

def save_feature(save_path, feature):    
    os.makedirs(os.path.dirname(save_path), exist_ok=True)

    print("[+]Save extracted feature to file : ", save_path)
    np.save(save_path, feature)

def extract_features(img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    img_data = image.img_to_array(img)
    img_data = np.expand_dims(img_data, axis=0)
    img_data = preprocess_input(img_data)
    feature = model.predict(img_data)
    return feature
    #save_feature(save_path, feature)
            

if __name__=="__main__":
    src = sys.argv[1]
    db = sys.argv[2]
    #img = np.load(src)
    clf = joblib.load(db + '/model.joblib')
    feature = extract_features(src)
    print("The object is: " + clf.predict(feature)[0])

